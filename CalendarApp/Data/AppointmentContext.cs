﻿using Microsoft.EntityFrameworkCore;
using CalendarApp.Models;

namespace CalendarApp.Data
{
    public class AppointmentContext : DbContext
    {

        //public AppointmentContext()
        //   : this(DbContextOptions < AppointmentContext > options)
        //{
        //    //parameterless constructor for scaffolding purposes
        //}

        public AppointmentContext(DbContextOptions<AppointmentContext> options)
            : base(options)
        {
        }

        public DbSet<Appointment> Appointment { get; set; }
    }
}