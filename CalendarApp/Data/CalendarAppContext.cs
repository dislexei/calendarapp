﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CalendarApp.Models;

namespace CalendarApp.Data
{
    public class CalendarAppContext : DbContext
    {
        public CalendarAppContext (DbContextOptions<CalendarAppContext> options)
            : base(options)
        {
        }

        public DbSet<CalendarApp.Models.Appointment> Appointment { get; set; }
    }
}
