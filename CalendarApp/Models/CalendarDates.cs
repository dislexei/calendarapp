﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalendarApp.Models
{
    public class CalendarDates
    {
        public DateTime[] offsetBeforeDates { get; set; }
        public DateTime[] currentMonthDates { get; set; }
        public DateTime[] offsetAfterDates { get; set; }
        public DateTime selectedDate { get; set; }
    }
}
