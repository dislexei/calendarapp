﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalendarApp.Models
{
    public class MyDate
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
    }

    public class NewApointment
    {
        public string Title { get; set; }
        public string Description{ get; set; }
        // public MyDate Date { get; set; }

        private MyDate _date;
        public MyDate Date
        {
            get
            {
                return _date;
            }
            set { _date = value; }
        }

        public DateTime GetDate()
        {
            var date = new DateTime(this.Date.Year, this.Date.Month + 1, this.Date.Day);
            return date;
        }
    }
}
