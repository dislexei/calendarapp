﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CalendarApp.Models
{
    public class Appointment
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime? Date { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

    }
}
