﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;
using CalendarApp.Models;

namespace CalendarApp.Controllers
{
    public class CalendarController : Controller
    {
        public IActionResult Index()
        {
            DateTime today = DateTime.Today;

            return RedirectToAction("ShowCalendar", new { year = DateTime.Today.Year, month = DateTime.Today.Month });
        }

        //gets current date
        private DateTime GetCurrentDate()
        {
            return DateTime.Today;
        }

        private int[] GetAllDaysInMonth(int year, int month) 
        {
            Enumerable.Range(1, DateTime.DaysInMonth(year, month)).ToArray();

            //var date = GetCurrentDate();
            //var date = new DateTime(year, month, 1);
            var dates = new int[DateTime.DaysInMonth(year, month)];

            for (int i = 0; i < dates.Length; i++) {
                dates[i] = i + 1;
            }

            return dates;
        }

        private DateTime[] GetAllDatetimesInMonth(int year, int month)
        {
            var dates = new DateTime[DateTime.DaysInMonth(year, month)];
            for (int i = 0; i < dates.Length; i++)
            {
                dates[i] = new DateTime(year, month, i+1);
            }
            return dates;
        }

        private DateTime[] GenerateOffsetBefore(int OffsetLenght, int currentYear, int currentMonth)
        {
            var offsetBefore = new DateTime[OffsetLenght];

            //gets dates in previous month
            DateTime[] daysPrevMonth;
            if (currentMonth > 1)
            {
                daysPrevMonth = GetAllDatetimesInMonth(currentYear, currentMonth - 1);
            }
            else
            {
                daysPrevMonth = GetAllDatetimesInMonth(currentYear - 1, 12);
            }

            //takes necessary amount from previous month
            for (int i = 0; i < OffsetLenght; i++)
            {
                offsetBefore[i] = daysPrevMonth[daysPrevMonth.Length - (i + 1)];
            }
            Array.Reverse(offsetBefore);

            return offsetBefore;
        }

        private DateTime[] GenerateOffsetAfter(int OffsetLenght, int currentYear, int currentMonth)
        {
            var offsetAfter = new DateTime[OffsetLenght];

            //gets dates in next month
            DateTime[] daysNextMonth;
            if (currentMonth != 12)
            {
                daysNextMonth = GetAllDatetimesInMonth(currentYear, currentMonth + 1);
            }
            else
            {
                daysNextMonth = GetAllDatetimesInMonth(currentYear + 1, 1);
            }

            //takes necessary amount from next month

            for (int i = 0; i < OffsetLenght; i++)
            {
                offsetAfter[i] = daysNextMonth[i];
            }

            return offsetAfter;
        }



        public RedirectToActionResult ShowPrevious(int currentYear, int currentMonth) {
            int prevYear = currentYear;
            int prevMonth = currentMonth;
            if (currentMonth == 1) {
                prevMonth = 12;
                prevYear = currentYear - 1;
            }
            else
            {
                prevMonth = currentMonth - 1;
            }
            return RedirectToAction("ShowCalendar", new { _year = prevYear, _month = prevMonth, _day = 1});
            //ShowCalendar(prevYear, prevMonth);
        }

        public RedirectToActionResult ShowNext(int currentYear, int currentMonth){
            int nextYear = currentYear;
            int nextMonth = currentMonth;
            if (currentMonth == 12)
            {
                nextMonth = 1;
                nextYear = currentYear + 1;
            }
            else
            {
                nextMonth = currentMonth + 1;
            }
            return RedirectToAction("ShowCalendar", new { _year = nextYear, _month = nextMonth, _day = 1});
            //ShowCalendar(nextYear, nextMonth);
        }
        //generates the calendar array and passes it to the view
        //will start with creating an array either 28 or 35 members long
        //1. add the required offset if the month does not start on Sunday
        //2. add all days that are relevant to the current month
        //3. add offset for the remaining positions in the array

        //4. replace offset with last days of the previous month, and first days of the next month
        public IActionResult ShowCalendar(int _year, int _month, int _day)
        {
            //string rawDate = _year + _month + _day;
            //DateTime selectedDate = DateTime.ParseExact(rawDate, "yyyy MM dd", CultureInfo.InvariantCulture);

            DateTime selectedDate = new DateTime(_year, _month, _day);

            int year = selectedDate.Year;
            int month = selectedDate.Month;
            int day = selectedDate.Day;

            //DateTime selectedDate = new DateTime(year, month, day);
            //gets the weekday of the first day in the month
            DateTime[] offsetBeforeDates;
            DateTime[] currentMonthDates;
            DateTime[] offsetAfterDates;

            DateTime firstDayOfMonth = new DateTime(year, month, 1);
            int firstWeekDay = (int)firstDayOfMonth.DayOfWeek;

            //generate offset before
            // 0 in DateTime.DayOfWeek represents Sunday if cast to integer
            if (firstWeekDay != 0)
            {
                offsetBeforeDates = GenerateOffsetBefore(firstWeekDay, year, month);
            }
            else offsetBeforeDates = new DateTime[0];

            //generate current month dates
            currentMonthDates = GetAllDatetimesInMonth(year, month);

            //generate offset after
            int offsetAfterLenght = 42 - (offsetBeforeDates.Length + currentMonthDates.Length);
            if (offsetAfterLenght > 0)
            {
                offsetAfterDates = GenerateOffsetAfter(offsetAfterLenght, year, month);
            }
            else offsetAfterDates = new DateTime[0];

            //ViewData["OffsetBefore"] = offsetBeforeDates;
            //ViewData["OffsetAfter"] = offsetAfterDates;
            //ViewData["CurrentMonthDates"] = currentMonthDates;
            //ViewData["Date"] = selectedDate;

            var m = new CalendarDates {
                offsetBeforeDates = offsetBeforeDates,
                currentMonthDates = currentMonthDates,
                offsetAfterDates = offsetAfterDates,
                selectedDate = selectedDate
            };

            return View(m);
        }
    }
}