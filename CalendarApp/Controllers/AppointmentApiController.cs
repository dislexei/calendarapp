﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CalendarApp.Models;
using CalendarApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalendarApp.Controllers
{
    //[Route("api/[controller]")]
    [Microsoft.AspNetCore.Mvc.Route("api/")]
    [ApiController]
    public class AppointmentApiController : ControllerBase
    {

        //private readonly Data.AppointmentContext _context;
        private readonly IApointmentServices _services;

        //constructor 
        public AppointmentApiController(IApointmentServices services)
        {
            _services = services;
        }

        //[HttpPost]
        //public ActionResult<Appointment> AddAppointmentItem(Appointment _appointment) {
        //    var appointment = _services.AddAppointmentItem(_appointment);


        //if (appointment == null) {
        //        return NotFound();
        //    }
        //    return appointment;
        //}


        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("GetAppointmentItems")]
        public ActionResult<Appointment> GetAppointmentItems()
        {
            var appointmentItems = _services.GetAppointments();

            return appointmentItems;
            //if (appointmentItems.Count == 0)
            //{
            //    return NotFound();
            //}
        }

        //sync get all appoinments
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("GetAllAppointments")]
        public ActionResult<List<Appointment>> GetAllAppointments()
        {
            var appointments = _services.GetAllAppointments();

            return appointments;
        }

        // async get all appoinments
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("GetAppointmentsByDate/{_year},{_month},{_day}")]
        public async Task<List<Appointment>> GetAppointmentsByDate(int _year, int _month, int _day)
        {
            return await _services.GetAllAppointments(_year, _month, _day);
        }

        //async method to delete an appointment with given id
        [Microsoft.AspNetCore.Mvc.HttpDelete]
        [Microsoft.AspNetCore.Mvc.Route("DeleteAppointmentByID/{_id}")]
        public async Task<ActionResult<Appointment>> DeleteAppointment(int _id)
        {
            return await _services.DeleteTodoItem(_id);
        }

        //async method to create an appoinmment without validation
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("CreateAppointment/")]
        public async Task<ActionResult<Appointment>> CreateAppointment([Microsoft.AspNetCore.Mvc.FromBody] NewApointment newApointment)
        {
            return await _services.CreateAppointment(newApointment);
        }

        //async method to create an appointment from form with server side validation
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("CreateAppointmentExperimental/")]
        public async Task<HttpResponseMessage> CreateAppointmentExperimental([Microsoft.AspNetCore.Mvc.FromBody] NewApointment newApointment)
        {
            var response = new HttpResponseMessage();

            //_services.CreateAppointment(newApointment);
            if (ServersideValidation(newApointment))
            {
                response.StatusCode = HttpStatusCode.Accepted;
                await _services.CreateAppointment(newApointment);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
                //response.StatusCode = HttpStatusCode.BadRequest;
                //response = new HttpRequestException();
            }
            return response;
        }

        //method to simulate a response message
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("TestResponse/")]
        public HttpResponseMessage TestResponse()
        {
            var response = new HttpResponseMessage();

            response.StatusCode = HttpStatusCode.Forbidden;

            return response;
        }

        //method to simulate an error while saving a new appointment
        private bool ServersideValidation(NewApointment newApointment) {
            if (newApointment.Description == "error" || newApointment.Title == "error") return false;
            else return true; 
        }
    }
}

//{'title':'ddd','description':'dddaaa', 'date':'123'}