﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CalendarApp.Controllers
{
    public class DayController : Controller
    {
        public IActionResult Index(DateTime date)
        {
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            string strMonthName = mfi.GetMonthName(date.Month).ToString();
            ViewData["Day"] = date.Day;
            ViewData["Month"] = strMonthName;
            ViewData["Year"] = date.Year;
            return View();
        }
    }
}