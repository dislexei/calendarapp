﻿
using CalendarApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalendarApp.Services
{
    public interface IApointmentServices
    {

        
        Appointment GetAppointments();
        List<Appointment> GetAllAppointments();
        Task<List<Appointment>> GetAllAppointments(int year, int month, int day);

        Task<ActionResult<Appointment>> DeleteTodoItem(int id);

        //void CreateAppointment(string json);
        Task<ActionResult<Appointment>> CreateAppointment(NewApointment newApointment);


        //Task<ActionResult<Appointment>> UpdateAppointment(int id, string json);
    }
}
