﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalendarApp.Models;
using Microsoft.EntityFrameworkCore;
using CalendarApp.Data;
using CalendarApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;



namespace CalendarApp.Services
{
    /*[ApiController]s*/
    public class AppointmentServices : IApointmentServices
    {
        private readonly AppointmentContext _context;

        public AppointmentServices(AppointmentContext context)
        {
            _context = context;
        }

        public Appointment GetAppointments()
        {
            return new Appointment {
                Id = 80,
                Name = "TestAppointment",
                Description = "This is a test appointment",
                Date = new DateTime(2019, 11, 4)
            };
        }

        //public List<Appointment> GetAllAppointments() {
        //    return _context.Appointment.ToList();
        //}

        public List<Appointment> GetAllAppointments()
        {
            return _context.Appointment.ToList();
        }

        public async Task<List<Appointment>> GetAppointmentById(int _id) {
            return await _context.Appointment
                .Where(n => n.Id == _id)
                .ToListAsync<Appointment>();
        }

        public async Task<List<Appointment>> GetAllAppointments(int _year, int _month, int _day)
        {
            DateTime date = new DateTime(_year, _month, _day);
            return await _context.Appointment
                .Where(n => n.Date == date)
                .ToListAsync<Appointment>();
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Appointment>> DeleteTodoItem(int id)
        {
            var appointment = await _context.Appointment.FindAsync(id);
            if (appointment == null)
            {
                return new NotFoundResult();
            }

            _context.Appointment.Remove(appointment);
            await _context.SaveChangesAsync();

            return appointment;
        }


        //public async Task<ActionResult<Appointment>> CreateAppointment(string json) {
        //    var appointment = new Appointment();
        //    _context.Appointment.Add(appointment);
        //    await _context.SaveChangesAsync();

        //    return new CreatedAtActionResult();

        //    //return await appointment;
        //}

        public async Task<ActionResult<Appointment>> CreateAppointment(NewApointment newApointment) {
            var appointment = new Appointment();
            appointment.Date = newApointment.GetDate();
            appointment.Name = newApointment.Title;
            appointment.Description = newApointment.Description;
            
            _context.Appointment.Add(appointment);
            await _context.SaveChangesAsync();
            return appointment;
            //_context.Add(appointment);
            //await _context.SaveChangesAsync();
        }

 
    }
}
